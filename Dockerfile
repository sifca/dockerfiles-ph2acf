# 
# Ph2_ACF Dockerfile
# https://github.com/duartej/dockerfiles/eudaq
#
# Creates the environment to run the Ph2_ACF with
# EUDAQ 1 support
#
FROM phusion/baseimage:focal-1.1.0
LABEL author="jorge.duarte.campderros@cern.ch" \ 
    version="v0.1" \ 
    description="Docker image to setup PH2_ACF middleware"
MAINTAINER Jordi Duarte-Campderros jorge.duarte.campderros@cern.ch

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# Place at the directory
WORKDIR /sw

# Install dependencies from system
RUN apt-get update \ 
  && install_clean --no-install-recommends software-properties-common \ 
  && install_clean --no-install-recommends \ 
   build-essential \
   git \ 
   cmake \ 
   libpugixml-dev \
   libusb-dev \ 
   libusb-1.0 \ 
   pkgconf \ 
   vim \ 
   g++ \
   gcc \
   gfortran \
   binutils \
   libxpm4 \ 
   libxft2 \ 
   libtiff5 \ 
   libtbb-dev \ 
   sudo \ 
   clang-tools \
   wireshark \
   tshark \ 
   rarpd \
   git-extras \
   python3-all-dev \
   erlang \ 
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# EUDAQ, boost and ROOT
COPY --from=duartej/eudaqv1:ph2_acf /eudaq/eudaq /sw/eudaq
COPY --from=duartej/eudaqv1:ph2_acf /eudaq/boost /sw/boost/
COPY --from=duartej/eudaqv1:ph2_acf /rootfr/root /rootfr/root

# Add daquser, allow to call sudo without password
RUN useradd -md /home/daquser -ms /bin/bash -G sudo daquser \ 
  && echo "daquser:docker" | chpasswd \
  && echo "daquser ALL=(ALL) NOPASSWD: ALL\n" >> /etc/sudoers 
# Give previously created folders ownership to the user
RUN chown -R daquser:daquser /sw 

## From here on, as daquser
USER daquser


ENV ROOTSYS="/rootfr/root"
# BE aware of the ROOT libraries
ENV LD_LIBRARY_PATH="/rootfr/root/lib"
ENV PYTHONPATH="/rootfr/root/lib"

ENV LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/sw/eudaq/lib:/sw/boost/lib"
ENV PATH="$PATH:/sw/eudaq/bin"


# uHAL libraries (cactus) ------
# IPbus compilation: 
# https://ipbus.web.cern.ch/doc/user/html/software/install/compile.html
# Environment variables for IPbus driver compilaton
RUN cd /sw \ 
   && sudo ln -s /usr/bin/python3 /usr/bin/python \
   && git clone --depth=1 -b master --recurse-submodules https://github.com/ipbus/ipbus-software.git \
   && cd /sw/ipbus-software \
   && export EXTERN_BOOST_INCLUDE_PREFIX="/sw/boost/include" \ 
   && export EXTERN_BOOST_LIB_PREFIX="/sw/boost/lib" \
   && export PYTHON="python3" \ 
   && LDFLAGS="-rpath -rpath-link" \
   && alias python='python3' \ 
   && make \ 
   # Install it in /opt/cactus
   && sudo make install 

ENV LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/cactus/lib"
ENV PATH="$PATH:/opt/cactus/bin"
# done uhal libraries

# Ph2_ACF compilation ------------

# ------------------------------ PROVISIONAL
# Variables to prepare compilation
ENV EUDAQDIR="/sw/eudaq"
ENV CompileWithEUDAQ=1
ENV CCompileWithTCUSB=1
# Standaline application with data streaming
ENV CompileForShep=true
ENV CompileForHerd=true
# Compile wiht TC USB library ?
ENV CompileWithTCUSB=false
ENV UseTCUSBforROH=false
ENV UseTCUSBTcpServer=false

# Stupding cmake ill-formed
ENV CACTUSROOT="/opt/cactus"
ENV CACTUSINCLUDE="/opt/cactus/include"
ENV CACTUSBIN="/opt/cactus/bin"
ENV CACTUSLIB="/opt/cactus/lib"
ENV BOOST_INCLUDE="/sw/boost/include"
ENV BOOST_LIB="/sw/boost/lib"
ENV PH2ACF_BASE_DIR="/sw/ph2acf"
ENV AMC13DIR="${CACTUSROOT}/include/amc13"
ENV AMC13DIR="${CACTUSROOT}/include/amc13"
# FLAGS!!!
ENV HttpFlag='-D__HTTP__'
ENV ZmqFlag='-D__ZMQ__'
ENV USBINSTFlag='-D__USBINST__'
ENV Amc13Flag='-D__AMC13__'
ENV TCUSBFlag='-D__TCUSB__'
ENV TCUSBforROHFlag='-D__ROH_USB__'
ENV TCUSBforSEHFlag='-D__SEH_USB__'
ENV TCUSBTcpServerFlag='-D__TCP_SERVER__'
ENV AntennaFlag='-D__ANTENNA__'
ENV UseRootFlag='-D__USE_ROOT__'
ENV MultiplexingFlag='-D__MULTIPLEXING__'
ENV EuDaqFlag='-D__EUDAQ__'
# FALTA Se tiene que descargar ENV ANTENNADIR="${CACTUSROOT}/include/amc13"
# Tambien este export USBINSTDIR=$PH2ACF_BASE_DIR/../Ph2_USBInstDriver
# ------------------------------ PROVISIONAL

# The software 
RUN cd /sw \
    && ls /opt/cactus \ 
    && git clone --recurse-submodules https://project_122778_bot:aLHWEzHvV-mDECuRHV8c@gitlab.cern.ch/duarte/Ph2_ACF.git ph2acf 

# Boost too new for cmake current version
COPY FindBoost.cmake /usr/share/cmake-3.16/Modules/FindBoost.cmake
# Same problem in the CMakeList.txt...
COPY NETWORKUTILS_CMakeLists.txt /sw/ph2acf/NetworkUtils/CMakeLists.txt

RUN cd ph2acf \
    && rm -rf /sw/ph2acf/build \
    && mkdir -p /sw/ph2acf/build \
    && cd /sw/ph2acf/build \
    && cmake .. -DCACTUS_ROOT="/opt/cactus" -DBOOST_ROOT="/sw/boost" -Wno-dev  \ 
    && make -j`grep -c processor /proc/cpuinfo` \
    && rm -rf /sw/ph2acf/build

ENV PATH="$PATH:/sw/ph2acf/bin"
#ENV LD_LIBRARY_PATH="$LD_LIBRARY_PATH:"

# Default command for starting the container, executed after the ENTRYPOINT
CMD ["bash"]

